package com.test.repository;

import com.test.entity.Pokemon;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

// this is the repository
// we created a class called CustomerRepository (because our database is called "customer")
@Repository
public interface PokemonRepository extends CrudRepository<Pokemon, Integer> {
}

// by extending crud repos, we have access to "save," "findAll", "count" etc.

// We have a repository for EVERY table in our database
// we have a repository and a matching entity