package com.test.services;

import com.sun.xml.bind.v2.model.core.ID;
import com.test.domain.PokemonRecap;
import com.test.entity.Pokemon;
import com.test.repository.PokemonRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

// You will need to create the various methods and look into Crud Repositories (similar to JPA)
@Service
@Slf4j // this is used for logging as you see below
public class PokemonServiceImpl implements PokemonService {

    @Autowired
    PokemonRepository pokemonRepository;

    @Override
    public Iterable<Pokemon> getAllPokemon() {
        log.info("Getting all customers");
        return pokemonRepository.findAll();
    }

    @Override
    public Pokemon getSpecificPokemon(int Id) {
        return pokemonRepository.findById(Id).get(); //this will throw an exception if the id doesn't exist
    }

    @Override
    public Pokemon postNewPokemon(Pokemon newPokemon) throws Exception {
        if (!pokemonRepository.existsById(newPokemon.getId())){
            return pokemonRepository.save(newPokemon);
        } else{
            throw new Exception("Pokemon already exists");
        }
    }

    @Override
    public Pokemon updatePokemon(int Id, Pokemon pokemon) throws Exception {
        if (pokemonRepository.existsById(Id)){
            return pokemonRepository.save(pokemon);
        } else{
            throw new Exception("Pokemon not found");
        }

    }

    @Override
    public String postBattle(int id1, int id2) throws Exception {
        if (pokemonRepository.existsById(id1) && pokemonRepository.existsById(id2)){
            Pokemon pokemon1 = pokemonRepository.findById(id1).get();
            Pokemon pokemon2 = pokemonRepository.findById(id2).get();
//            return pokemon2;
            PokemonRecap result = Pokemon.battle(pokemon1, pokemon2);
            result.getWinner().setTotalWins(result.getWinner().getTotalWins()+1);
            result.getWinner().setStrength(result.getWinner().getStrength()+2);
            result.getWinner().setHealth(result.getWinner().getHealth()+1);
            result.getLoser().setTotalLosses(result.getLoser().getTotalLosses()+1);
            if (result.getLoser().getLevel() > result.getWinner().getLevel()) {
                result.getWinner().setLevel(result.getWinner().getLevel()+1);
                result.getLoser().setHealth(result.getLoser().getHealth()-1);
                result.getLoser().setStrength(result.getLoser().getStrength()-1);
            } else if (result.getLoser().getLevel() < result.getWinner().getLevel()){
                result.getLoser().setLevel(result.getLoser().getLevel()-1);
                result.getLoser().setHealth(result.getLoser().getHealth()+1);
                result.getLoser().setStrength(result.getLoser().getStrength()+1);
            }


            pokemonRepository.save(result.getWinner());
            pokemonRepository.save(result.getLoser());

            return result.getWinner().getName() + " is the winner!";

//            return pokemonRepository.save(pokemon);
        } else{
            throw new Exception("Pokemon not found");
        }

    }



    @Override
    public void deletePokemon(int Id) {
        pokemonRepository.deleteById(Id);
    }

}
