package com.test.services;

import com.test.domain.PokemonRecap;
import com.test.entity.Pokemon;

public interface PokemonService {
    // Get all
    public Iterable<Pokemon> getAllPokemon();
    // Get specific
    public Pokemon getSpecificPokemon(int Id);
    // POST new customer
    public Pokemon postNewPokemon(Pokemon newPokemon) throws Exception;
    // POST new customer
    public String postBattle(int pokemon1, int pokemon2) throws Exception;
    // Update (PUT) new customer
    public Pokemon updatePokemon(int Id, Pokemon pokemon) throws Exception;
    // DELETE customer
    public void deletePokemon(int Id);
}
