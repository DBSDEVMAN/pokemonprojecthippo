package com.test.entity;


import com.test.domain.Modifiers;
import com.test.domain.PokemonRecap;
import com.test.domain.Types;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity //need to create the entity in memory; we are defining the structure to hold the data in memory, that we pull from the database
@Table(name="pokemon") //this is an entity; database table represented as a class
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class Pokemon {
    @Id //primary key
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    Integer id;

    @Column(name="name")
    String name;

    @Column(name="strength")
    Integer strength;

    @Column(name="health")
    Integer health;

    @Column(name="description")
    String description;

    @Column(name="totalWins")
    Integer totalWins;

    @Column(name="totalLosses")
    Integer totalLosses;

    @Column(name="level")
    Integer level;

    @Column(name="speed")
    Integer speed;

    @Column(name="type")
    String type;

    @Column(name="maxHealth")
    Integer maxHealth;



    ////code from old project
    public Pokemon(int health, int strength, int speed, String name, Types type) {
        if (health > 300 || health < 1) {
            throw new IllegalArgumentException("Health must be between 1 and 300");
        }
        if (strength > 300 || strength < 1) {
            throw new IllegalArgumentException("Strength must be between 1 and 300");
        }
        if (speed > 300 || speed < 1) {
            throw new IllegalArgumentException("Speed must be between 1 and 300");
        }
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException("Must input a name!");
        }

        this.health = health;
        this.strength = strength;
        this.speed = speed;
        this.name = name;
        this.maxHealth = health;
        this.type = type.toString();
    }

    // Method (boolean, true or false)
    public boolean isFainted() {
        // Declare whether health is less than or equal to zero
        return health <= 0;
    }

    // Method (don't need to return anything)
    public void takeDamage(int damage) {
        health = health - damage;

        if (health < 0) {
            health = 0;
        }

        // get name

    }

    public String getName() {
        return name;

    }

    // private because only the battle can do attacks
    private static void attack(Pokemon attacker, Pokemon defender, List<String> recap) {
        Modifiers multiplier = determinedEffectiveness(attacker.type, defender.type);
        int damage = (int)(attacker.strength * multiplier.multiplier);

        defender.takeDamage(damage);
        switch(multiplier) {
            case EFFECTIVE:
                recap.add(attacker.name + " attacked, and it was super effective!");
                break;
            case NEUTRAL:
                recap.add(attacker.name + " attacked");
                break;
            case NOT_EFFECTIVE:
                recap.add(attacker.name + " attacked, but it wasn't very effective :(");
                break;
        }
        recap.add(defender.name + " took " + damage + " damage and has " + defender.health + " points of health remaining.");
    }

    private static Modifiers determinedEffectiveness(String attack, String defense) {
        switch (attack) {
            case "Fire":
                switch (defense) {
                    case "Fire":
                        return Modifiers.NEUTRAL;
                    case "Water":
                        return Modifiers.NOT_EFFECTIVE;
                    case "Grass":
                        return Modifiers.EFFECTIVE;
                }
                ;
                break;

            case "Water":
                switch (defense) {
                    case "Fire":
                        return Modifiers.EFFECTIVE;
                    case "Water":
                        return Modifiers.NEUTRAL;
                    case "Grass":
                        return Modifiers.NOT_EFFECTIVE;
                }
                ;
                break;

            case "Grass":
                switch (defense) {
                    case "Fire":
                        return Modifiers.NOT_EFFECTIVE;
                    case "Water":
                        return Modifiers.EFFECTIVE;
                    case "Grass":
                        return Modifiers.NEUTRAL;
                }
                ;
                break;
        }
        return Modifiers.NEUTRAL;
    }

    // Write a program to handle a battle between two pokemon
    // We want this to show what the health is through each round of a battle
    // Be creative with this
    // HINT : You will need to make sure you battle until 1 pokemon has 0 health
    // (Think of possible loops to use)
    public static PokemonRecap battle(Pokemon pokemon1, Pokemon pokemon2) {
        // enter code here
        Pokemon pokemonFast;
        Pokemon pokemonSlow;
        List<String> records = new ArrayList<String>();

        if (pokemon1.speed > pokemon2.speed) {
            pokemonFast = pokemon1;
            pokemonSlow = pokemon2;
        } else {
            pokemonFast = pokemon2;
            pokemonSlow = pokemon1;
        }
        // check to see while the pokemons are both fainted or not
        while (!pokemonFast.isFainted() && !pokemonSlow.isFainted()) {
            attack(pokemonFast, pokemonSlow, records);

            if (!pokemonSlow.isFainted()) {
                attack(pokemonSlow, pokemonFast, records);
            }
        }

        Pokemon winner, loser;

        // Declare a pokemon winner
        if (pokemonFast.isFainted()) {
            winner = pokemonSlow;
            loser = pokemonFast;
        } else {
            winner = pokemonFast;
            loser = pokemonSlow;

        }
        records.add(loser.name + " has fainted and lost the battle");
        records.add(winner.name + " has won the battle!!");

        return new PokemonRecap(winner, loser, records);
    }
}
