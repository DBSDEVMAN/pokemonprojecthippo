package com.test.controller;

import com.test.entity.Pokemon;
import com.test.services.PokemonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

// *********** GO TO SETTINGS, Plugins and install LOMBOK **************
// Pokemon class (instructor copy)
// LAB:
//      Go through and create 5 methods
//      using each of the Rest Calls we just learned (Get, post, put, delete)
// GIVEN:
//      You are given the entity and the repository class already setup for you
// GOAL:
//      By the end of this you should be able to get, add, update, and delete Pokemon from the database!


@RestController
@RequestMapping(path = "/pokemon")
public class PokemonController {

    @Autowired
    PokemonService pokemonService;

    @GetMapping("")
    public ResponseEntity<Iterable<Pokemon>> getAllPokemon() {
        return new ResponseEntity<Iterable<Pokemon>>(pokemonService.getAllPokemon(), HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<Pokemon> postNewPokemon(@RequestBody Pokemon newPokemon) throws Exception{
        return new ResponseEntity<Pokemon>(pokemonService.postNewPokemon(newPokemon), HttpStatus.OK);
    }

    @GetMapping("/{Id}")
    public ResponseEntity<Pokemon> getSpecificPokemon(@PathVariable int Id){
        return new ResponseEntity<Pokemon>(pokemonService.getSpecificPokemon(Id), HttpStatus.OK);
    }

    ///start here!
    @PutMapping("/{Id}")
    public ResponseEntity<Pokemon> updatePokemon(@PathVariable int Id, @RequestBody Pokemon Pokemon) throws Exception{
        return new ResponseEntity<Pokemon>(pokemonService.updatePokemon(Id, Pokemon), HttpStatus.OK);
    }

    @DeleteMapping("/{Id}")
    public ResponseEntity<String> deletePokemon(@PathVariable int Id) {
        pokemonService.deletePokemon(Id);
        return new ResponseEntity<String>("Successfully deleted record!", HttpStatus.OK);
    }

    @PostMapping("/battle/{id1}/{id2}")
    public ResponseEntity<String> postBattle(@PathVariable int id1, @PathVariable int id2) throws Exception{
        return new ResponseEntity<String>(pokemonService.postBattle(id1, id2), HttpStatus.OK);
    }

}

