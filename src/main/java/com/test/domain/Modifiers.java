package com.test.domain;

public enum Modifiers {
    EFFECTIVE(1.5),
    NEUTRAL(1.0),
    NOT_EFFECTIVE(0.5);
    // create multiplier field
    public final double multiplier;

    private Modifiers(double multiplier){
        this.multiplier = multiplier;

    }
}
