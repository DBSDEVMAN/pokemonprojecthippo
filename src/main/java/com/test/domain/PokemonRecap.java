package com.test.domain;

import com.test.entity.Pokemon;

import java.util.List;

public class PokemonRecap{
    private Pokemon winner;
    private Pokemon loser;
    // list of strings that have a play by play
    private List<String> recap;

    // constructor
    public PokemonRecap(Pokemon winner, Pokemon loser, List<String> recap){
        this.winner = winner;
        this.loser = loser;
        this.recap = recap;

    }

    public List<String> getRecap(){
        return recap;
    }
    public Pokemon getWinner(){ return winner; }
    public Pokemon getLoser(){ return loser; }

}
