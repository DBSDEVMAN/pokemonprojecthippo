# README #

## Pokemon Battle: Web Services ##

Group members

* Abbie Tolon
* Raquel Poff
* Joey Perri

## Purpose ##
To create a program where pokemon are added to a SQL database. We can then select two pokemon from the database to battle each other. Pending battle results, the database will be updated accordingly. 

Program is written in Java. Database is through postgres. Postman utilized for GET/POST/PUT/DELETE requests.

**Sample Requests**

*Get all*

GET http://localhost:8081/pokemon

*Get specific*

GET http://localhost:8081/pokemon/1

*Battle*

PUT http://localhost:8081/pokemon/1/2